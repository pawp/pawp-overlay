Pawpy Overlay
================

This is a overlay with a bunch of software not available in Gentoo repositories

There's ebuilds made by myself and other gathered from the internet and made
some personal modifications. The follow repositories was used (sorry if I forgot something):

:Abendbrot: https://github.com/stefan-gr/abendbrot/ 
:Raiagent: https://github.com/leycec/raiagent/
:Steam-Overlay: https://github.com/anyc/steam-overlay
:Gamerlay: https://cgit.gentoo.org/proj/gamerlay.git/
:Pentoo: https://github.com/pentoo/pentoo-overlay
:Tastytea: https://schlomp.space/tastytea/overlay
:menelkir: https://gitlab.com/menelkir/gentoo-overlay

=================================
How to Contribute to this Overlay
=================================

:author: pawp
:contact: pawp@protonmail.com
:language: English
